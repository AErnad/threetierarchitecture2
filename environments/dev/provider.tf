provider "aws" {
  region                   = var.region
  shared_credentials_files = ["/Users/mac/.aws/credentials"]
}
