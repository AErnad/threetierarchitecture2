# profile
variable "region" {
  type    = string
  default = "us-east-1"
}

variable "aws_profile" {
  type    = string
  default = "default"
}

variable "availability_zone" {
  type    = string
  default = "us-east-1a"
}

data "aws_key_pair" "key" {
  key_name = "fison-key-pair"
  filter {
    name   = "tag:mykey"
    values = ["myvalue"]
  }
}

# networking
variable "access_ip" {
  type = string
}

variable "public_sn_count" {
  type    = number
  default = 2
}

variable "private_sn_count" {
  type    = number
  default = 2
}

variable "nw_azs" {
  type    = number
  default = 2
}

# DB
variable "db_name" {
  type = string
}

variable "db_instance" {
  type    = string
  default = "db.t2.micro"
}

variable "db_storage" {
  type    = number
  default = 10
}

variable "db_engine_version" {
  type    = string
  default = "5.7"
}

variable "dbuser" {
  type      = string
  sensitive = true
}

variable "dbpassword" {
  type      = string
  sensitive = true
}

variable "db_identifier" {
  type    = string
  default = "three-tier-db"
}

# compute
variable "instance_type" {
  type    = string
  default = "t2.micro"
}

variable "bastion_instance_count" {
  type    = number
  default = 1
}

# loadbalancing
variable "tg_port" {
  type    = number
  default = 80
}

variable "listener_port" {
  type    = number
  default = 80
}

variable "tg_protocol" {
  type    = string
  default = "HTTP"
}

variable "listener_protocol" {
  type    = string
  default = "HTTP"
}

variable "lb_azs" {
  type    = number
  default = 2
}
