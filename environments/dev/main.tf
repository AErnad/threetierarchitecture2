locals {
  vpc_cidr = "10.123.0.0/16"
}

module "networking" {
  source           = "../../modules/networking"
  vpc_cidr         = local.vpc_cidr
  access_ip        = var.access_ip
  public_sn_count  = var.public_sn_count
  private_sn_count = var.private_sn_count
  db_subnet_group  = true
  availabilityzone = var.availability_zone
  azs              = var.nw_azs
}

module "compute" {
  source                 = "../../modules/compute"
  frontend_app_sg        = module.networking.frontend_app_sg
  backend_app_sg         = module.networking.backend_app_sg
  bastion_sg             = module.networking.bastion_sg
  public_subnets         = module.networking.public_subnets
  private_subnets        = module.networking.private_subnets
  bastion_instance_count = var.bastion_instance_count
  instance_type          = var.instance_type
  key_name               = data.aws_key_pair.key.key_name
  lb_tg_name             = module.loadbalancing.lb_tg_name
  lb_tg                  = module.loadbalancing.lb_tg
}

module "database" {
  source               = "../../modules/database"
  db_storage           = var.db_storage
  db_engine_version    = var.db_engine_version
  db_instance_class    = var.db_instance
  db_name              = var.db_name
  dbuser               = var.dbuser
  dbpassword           = var.dbpassword
  db_identifier        = var.db_identifier
  skip_db_snapshot     = true
  rds_sg               = module.networking.rds_sg
  db_subnet_group_name = module.networking.db_subnet_group_name[0]
}

module "loadbalancing" {
  source            = "../../modules/loadbalancing"
  lb_sg             = module.networking.lb_sg
  public_subnets    = module.networking.public_subnets
  tg_port           = var.tg_port
  tg_protocol       = var.tg_protocol
  vpc_id            = module.networking.vpc_id
  app_asg           = module.compute.app_asg
  listener_port     = var.listener_port
  listener_protocol = var.listener_protocol
  azs               = var.lb_azs
}
